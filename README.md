# README #

Forked from https://github.com/ArduPilot/apm_planner to retain compatibility for the rest of the setup

## Installation

Install prerequisites
````
sudo apt-get update
sudo apt-get install qt5-qmake qt5-default \
  qtscript5-dev libqt5webkit5-dev libqt5serialport5-dev \
  libqt5svg5-dev qtdeclarative5-qtquick2-plugin libqt5serialport5-dev \
  libqt5opengl5-dev qml-module-qtquick-controls
sudo apt-get install git libsdl1.2-dev  libsndfile-dev \
  flite1-dev libssl-dev libudev-dev libsdl2-dev python-serial python-pexpect
````

Clone repository
````
git clone https://bitbucket.org/utiasfsc/apm-planner.git ~/apm_planner
````

Build from source
````
cd ~/apm_planner
qmake apm_planner.pro
make
````
Note: don't use the ````make install```` command, it will result in some path error or priviledge error in opening serial connection

Granting permission to modify/read serial connection
````
sudo usermod -a -G dialout $USER
````


## Launch
````
./apm_planner/release/apmplanner2
````
